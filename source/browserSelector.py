#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Original author and repo: https://gitlab.com/ToS0/browserselector

import sys

from BrowserSelector import BrowserSelector


def main():
    url_to_open = sys.argv[1] if len(sys.argv) > 1 else None

    app = BrowserSelector(url_to_open)
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
