import sys

try:
    from PyQt6 import QtCore, QtGui, QtWidgets
except (ModuleNotFoundError, ImportError) as e:
    if not isinstance(e, ModuleNotFoundError):
        # If PyQt6 is found, but is broken or incomplete, display a message and try PyQt5 too
        print(
            'Found PyQt6, but it cannot be imported.',
            f'Error: {e}',
            'Trying PyQt5.',
            file=sys.stderr,
            sep='\n',
        )
    from PyQt5 import QtCore, QtGui, QtWidgets
