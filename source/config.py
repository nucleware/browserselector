import json
import os
from typing import Optional, Any, List

from xdg.DesktopEntry import DesktopEntry

from PyQt5or6 import QtCore
from desktop_file import read_desktop_entry

config_dir = (
    QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.StandardLocation.ConfigLocation)[0] +
    '/browserselector'
)


def get_config_path(filename: str) -> str:
    return f'{config_dir}/{filename}'


class JsonConfigFile:
    def __init__(self, filename: str, default: Any = None):
        self.data = default
        self.filename = filename
        self.default = default
        self.load()

    def load(self) -> None:
        self.data = self.default

        if not os.path.isfile(self.filename):
            return

        with open(self.filename) as input_file:
            try:
                self.data = json.load(input_file)
            except json.JSONDecodeError:
                pass

    def save(self) -> None:
        if self.data is None:
            if os.path.exists(self.filename):
                os.unlink(self.filename)
            return

        os.makedirs(os.path.dirname(self.filename), 0o755, exist_ok=True)
        with open(self.filename, 'w') as json_file:
            json.dump(self.data, json_file, indent=4, separators=(',', ': '))


class SavedBrowsers(JsonConfigFile):
    def __init__(self, urls_path: Optional[str] = None):
        if urls_path is None:
            urls_path = get_config_path('urls.json')
        super().__init__(urls_path, [])

    def get_remembered_browser(self, domain: str) -> Optional[DesktopEntry]:
        for entry in self.data:
            if entry['url'] == domain:
                desktop_entry = read_desktop_entry(entry['browser'])
                return desktop_entry
        return None

    def remember(self, domain: str, desktop_entry: DesktopEntry) -> None:
        self.forget(domain)
        self.data.append({
            "url": domain,
            "browser": desktop_entry.filename,
        })

    def forget(self, domain: str) -> None:
        self.data = list(filter(lambda entry: entry.url != domain, self.data))


class AppConfig(JsonConfigFile):
    def __init__(self, config_path: Optional[str] = None):
        if config_path is None:
            config_path = get_config_path('config.json')
        super().__init__(config_path)

    def is_hidden_browser(self, filename: str) -> bool:
        basename = os.path.basename(filename)
        if basename.endswith('.desktop'):
            basename = basename[:-len('.desktop')]
        hidden_browsers = self.get_hidden_browsers()
        return basename in hidden_browsers

    def get_hidden_browsers(self) -> List[str]:
        return self._get('hidden_browsers', [])

    def set_hidden_browsers(self, hidden_browsers: List[str]) -> None:
        self.data['hidden_browsers'] = hidden_browsers

    def add_hidden_browser(self, filename: str) -> None:
        hidden_browsers = self.get_hidden_browsers()
        if filename not in hidden_browsers:
            hidden_browsers.append(filename)
            self.set_hidden_browsers(hidden_browsers)

    def remove_hidden_browser(self, filename: str) -> None:
        hidden_browsers = self.get_hidden_browsers()
        if filename in hidden_browsers:
            hidden_browsers = [browser for browser in hidden_browsers if browser != filename]
            self.set_hidden_browsers(hidden_browsers)

    def include_no_display_browsers(self) -> bool:
        return self._get('include_no_display_browsers', False)

    def set_include_no_display_browsers(self, include_no_display_browsers: bool) -> None:
        self._set('include_no_display_browsers', include_no_display_browsers)

    def _set(self, key: str, value: Any) -> None:
        if self.data is None:
            self.data = {}
        self.data[key] = value

    def _get(self, key: str, default: Any = None) -> Any:
        if self.data is None:
            return default
        return self.data.get(key, default)
