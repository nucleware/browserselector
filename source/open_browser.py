import os
import shlex
import sys
from typing import Optional, List

from xdg.DesktopEntry import DesktopEntry


def open_browser(desktop_entry: DesktopEntry, urls: Optional[List[str]] = None):
    if urls is None:
        urls = []

    argv: list[str] = shlex.split(desktop_entry.getExec())

    try:
        print(f'Opening URLs: {urls} with {desktop_entry.getExec()}', file=sys.stderr)
    except BrokenPipeError:
        pass

    if '%U' in argv:
        # %U allows a list of URLs to be substituted
        subst_index = argv.index('%U')
        commands = [argv[:subst_index] + urls + argv[subst_index + 1:]]
    elif '%u' in argv:
        # %u allows only a single URL to be substituted; several URLs means several instances will be launched
        subst_index = argv.index('%u')

        if len(urls) > 0:
            commands = []
            for url in urls:
                argv[subst_index] = url
                commands.append(argv)
        else:
            commands = [argv[:subst_index] + argv[subst_index + 1:]]
    else:
        # no substitution performed
        commands = [argv]

    for command in commands:
        pid = os.fork()
        if pid == 0:
            os.execvp(argv[0], command)
            # if execvp fails, exit immediately, without cleanup
            os._exit(127)
