from typing import Optional

import xdg
from xdg.DesktopEntry import DesktopEntry


def read_desktop_entry(filename: str) -> Optional[DesktopEntry]:
    desktop_entry = DesktopEntry()
    try:
        desktop_entry.parse(filename)
    except (xdg.Exceptions.ParsingError, xdg.Exceptions.DuplicateGroupError, xdg.Exceptions.DuplicateKeyError):
        return None

    return desktop_entry
