import os.path
import sys
from functools import partial
from typing import Optional

from xdg import DesktopEntry
from xdg import IconTheme

from PyQt5or6 import QtCore, QtWidgets, QtGui
from config import SavedBrowsers, AppConfig
from get_browsers import get_browsers
from open_browser import open_browser
from parse_url import parse_url


class BrowserSelector:
    def __init__(self, url_to_open: Optional[str]):
        self.app_config = AppConfig()

        self.url_to_open = url_to_open
        self.saved_browsers = SavedBrowsers()
        self.available_browsers = get_browsers(include_no_display=self.app_config.include_no_display_browsers())

        self.parsed_url = parse_url(url_to_open)
        self.browser_selector = None

    def exec(self) -> int:
        if self.parsed_url is not None:
            remembered_browser = self.saved_browsers.get_remembered_browser(self.parsed_url.domain)
            if remembered_browser is not None:
                self.open_browser(remembered_browser)
                return 0

        self.remove_hidden_browsers()

        app = QtWidgets.QApplication(sys.argv)
        self.browser_selector = SelectorWidget(self)
        return app.exec()

    def open_browser(self, browser_desktop_entry: DesktopEntry) -> None:
        # hit it, baby
        open_browser(browser_desktop_entry, [self.url_to_open] if self.url_to_open else None)

    def remember(self, browser_desktop_entry: DesktopEntry) -> None:
        self.saved_browsers.remember(self.parsed_url.domain, browser_desktop_entry)
        self.saved_browsers.save()

    def remove_hidden_browsers(self) -> None:
        hidden_browsers = self.app_config.get_hidden_browsers()
        self.available_browsers = [
            browser
            for browser in self.available_browsers
            if os.path.basename(browser.filename)[:-len('.desktop')] not in hidden_browsers
        ]


class SelectorWidget(QtWidgets.QWidget):
    iconSize = {'w': 48, 'h': 48}

    def __init__(self, browser_selector: BrowserSelector):
        super().__init__()

        self.browser_selector = browser_selector

        self.setup_window()
        self.show()

    def setup_window(self) -> None:
        cbox = None
        if self.browser_selector.parsed_url is not None:
            cbox = QtWidgets.QCheckBox()
            cbox.setText("Remember '" + self.browser_selector.parsed_url.label + "'?")

        form = QtWidgets.QFormLayout()
        if cbox is not None:
            form.addWidget(cbox)

        # create the buttons for the browsers
        buttons = []
        for browser in self.browser_selector.available_browsers:
            button = self.create_button(browser)
            button.clicked.connect(partial(self.button_clicked, browser, cbox))
            buttons.append(button)

        # and the layout
        outer_layout = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()

        # the buttons
        for button in buttons:
            hbox.addWidget(button)

        # create the layout
        outer_layout.addLayout(hbox)
        outer_layout.addLayout(form)
        self.setLayout(outer_layout)

        # focus to 1st button to allow keyboard CR
        buttons[0].setDefault(True)
        buttons[0].setFocus()

        # move the window to the cursor
        pos = QtGui.QCursor().pos()
        self.move(pos.x() - (self.iconSize['w'] + 0), pos.y() - self.iconSize['h'] - 30)
        self.setWindowTitle("Open with")

    def button_clicked(self, browser: DesktopEntry, cbox: Optional[QtWidgets.QCheckBox]) -> None:
        # remember browser choice for the next time
        if cbox is not None and cbox.isChecked():
            self.browser_selector.remember(browser)

        self.browser_selector.open_browser(browser)
        self.close()

    def create_button(self, desktop_entry: DesktopEntry) -> QtWidgets.QPushButton:
        icon = QtGui.QIcon()
        # set the data from the desktop file
        icon.addPixmap(QtGui.QPixmap(IconTheme.getIconPath(desktop_entry.getIcon())))

        button = QtWidgets.QPushButton("")

        button.setIcon(icon)
        button.setIconSize(QtCore.QSize(self.iconSize['w'], self.iconSize['h']))
        button.setToolTip(desktop_entry.getComment())

        return button
