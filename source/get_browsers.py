import os.path
from glob import glob
from typing import List

from xdg.DesktopEntry import DesktopEntry

from PyQt5or6 import QtCore
from desktop_file import read_desktop_entry


def get_browsers(*, include_no_display: bool = False) -> List[DesktopEntry]:
    mime_app_dirs = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.StandardLocation.ApplicationsLocation)

    browsers = []
    seen = {}
    for mime_app_dir in mime_app_dirs:
        desktop_files = glob(mime_app_dir + '/*.desktop')

        for desktop_file in desktop_files:
            # We use the basename of the .desktop file to determine if we'll use it
            # The first file wins. This allows the user to override a .desktop file from a system directory
            # (e.g. /usr/share/applications/firefox.desktop) by placing a file with the same name in their local
            # applications directory (e.g. ~/.local/share/applications/firefox.desktop)
            base_name = os.path.basename(desktop_file)
            if base_name in seen:
                continue

            desktop_entry = read_desktop_entry(desktop_file)
            if desktop_entry is None:
                continue

            if desktop_entry.getStartupWMClass() == 'BrowserSelector.py':
                continue

            if not include_no_display and desktop_entry.getNoDisplay():
                continue

            seen[base_name] = True

            categories: list[str] = desktop_entry.getCategories()
            mime_types: list[str] = desktop_entry.getMimeTypes()

            if (
                    'WebBrowser' in categories
                    and [mime_type
                         for mime_type in mime_types
                         if mime_type in ['x-scheme-handler/http', 'x-scheme-handler/https']
                         ]
            ):
                browsers.append(desktop_entry)

    return browsers
