import os
from typing import Optional
from urllib.parse import urlparse


class ParsedUrl:
    def __init__(self):
        self.domain = None
        self.label = None

    def __iter__(self):
        yield self.domain
        yield self.label


def parse_url(url: Optional[str]) -> Optional[ParsedUrl]:
    if url is None:
        return None

    result = ParsedUrl()

    parsed_url = urlparse(url)

    if parsed_url.scheme in ['file', '']:
        # if it is a file, show (shortened) name after last /
        result.domain = os.path.basename(parsed_url.path)
        (file, ext) = os.path.splitext(result.domain)
        file = file[:10] + ('~' if len(file) > 10 else '') + file[-10:]
        result.label = file + "." + ext

        return result

    # show URL domain
    result.domain = parsed_url.netloc
    result.label = ('~' if len(result.domain) > 20 else '') + result.domain[-20:]

    return result
