# Linux Browser Selector
Choose your web browser for links in any app on Linux, and remember your choice if needed. Customize the browsers (or apps) you want to use.
<br><br>
![](images/screenshot.png)
<br>
Above, a link to GitLab is opened, and several browsers are offered.

## Description
Whenever you click on a link (or web-related file), you can choose which web browser shall be used to open it. By clicking the "Remember" checkbox, the script will remember your choice for the next time for this particular domain or file, so the script works silently in the background.
<br>
It is basically a replacement of your default web browser in the first place, so you can use it to subsequently call and run different browsers instead, e.g. one for anonymous surfing, one for online accounts, one for secure banking, another one for coding, testing and so on.
<br>
The script is a combination of the functions from [Junction](https://github.com/sonnyp/Junction) and [Picker](http://www.cesarolea.com/posts/browser-picker-linux/) on Linux -- much like [Choosy](https://www.choosyosx.com/) on Mac, or [BrowseRouter](https://github.com/slater1/BrowseRouter) on Win.

## Installation
* Download (and extract) or clone the repo (see buttons above on the right hand side)
* run the .sh file in `install/` to install
   ```
   ./install/browserSelectorInstall.sh
   ```
  
This will install:
* Python 3
* pyxdg
* PyQT5 or PyQT6 (preferring the latter)

It will also install the `browserSelector.desktop` file to `~/.local/share/applications/`, where your DE can find it.

* test it e.g. with `./browserSelector.py https://gitlab.com`, or `./browserSelector.py ../tests/ab.html`

## Configure as Default Application

### Automatic

The easiest way to use the browser selector is to configure your Desktop Environment (e.g. GNOME, KDE) to use `Browser Selector`
as the default web browser. After you run the install script, your Desktop Environment should offer `Browser Selector`
in the list of apps available to be the Web Browser.

### Manual

* add `browserSelector.desktop` to the following protocol entries in `~/.config/mimeapps.list`:
    * in section `[Default Applications]` to (separated by semicolon)
        * `text/html`
        * `text/xml`
        * `image/webp`
        * `x-scheme-handler/http`
        * `x-scheme-handler/https`
        * `x-scheme-handler/ftp`
        * `application/xhtml_xml`
    * and do it all over again for the same entries in `[Added Associations]` further down the file
        * so, for example, you have twice something like this in your file: `text/html=codium.desktop;browserSelector.desktop;`
* now logout from your desktop session and login again to have your changes loaded
* finally link your Default web browser setting to `Browser Selector`
    * in Gnome: `System settings` / `Default applications` / `Web`
    * in KDE: `Settings` / `Standard Components`
    * you may also run `xdg-settings set default-web-browser browserSelector.desktop` from your shell instead
* test it e.g. with `xdg-open https://gitlab.com`, or `xdg-open ../tests/ab.html`
 
## Usage
* just click a link (or file) in any app and then choose your browser, right where your mouse pointer is -- **Enjoy**!
    * beside using the mouse you can also use your left/right cursor keys and the space key to open your browser
* the remembered domains are saved in `~/.config/browserselector/urls.json`
    * you don't need to add multiple entries for (or w/o) `www`, or for protocols like `http`/`https` etc. -- domain and top-level is enough (e.g. just `gitlab.com`)
    * ports are treated like different domains (e.g. `localhost:8080` is different to `localhost`)
    * paths in URLs and files are generally ignored on purpose

## Configuration
There are two files in `~/.config/browserselector/`:

* `urls.json`

   A list of domain+browser pairs

   Example:

   ```json
   [
     {
       "url": "google.com",
       "browser": "/usr/share/applications/google-chrome.desktop"
     }
   ]
   ```

* config.json

   A general app configuration file.

   It includes a list of hidden browsers for now, but it could include other settings in the future.
   The entries are the basename without the path or the `.desktop` extension.

   Example:

   ```json
   {
     "include_no_display_browsers": false,
     "hidden_browsers": [
        "microsoft-edge"
     ]
   }
   ```

## Known Issues
In case your browser doesn't open, its process may hang for some reason -- then simply check and kill it, e.g. with `ps -fe | grep chrome` and `pkill -9 chrome`

## 🆘 Need help?
Open an issue on the Gitlab repository page.

## Roadmap
* Ability to configure excluded browsers and remembered domains in the GUI
* Distro packages (e.g. deb, rpm)
* Multi-profile awareness: ability to select which browser profile to use

   Firefox didn't use to allow more than one instance without the `-no-remote` parameter
   but [this changed in 2019](https://groups.google.com/g/mozilla.dev.platform/c/Vz-L_of5WZE/m/myooqd_ABAAJ).
   Now I have to come up with a good way to present this.

   I do not know what Chrome is up to in this regard.

## Contributing
Forks or Merge requests are welcome!

## License
[MIT](LICENSE)

## Prior art
* First idea and implementation is from AiwendilH @ [Reddit](https://www.reddit.com/r/linux/comments/2lgokr/looking_for_an_app_that_lets_you_choose_web/)
* This project was forked from `git@gitlab.com:ToS0/browserselector.git`
