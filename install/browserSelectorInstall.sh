#!/bin/bash

set -e

Dir=$(dirname "$0")

apt update

PyQtPackage=$(apt-cache search --names-only '^python3-pyqt[56]$' | awk '{print $1}' | sort -u | tail -1)

if [[ -z "$PyQtPackage" ]]; then
  echo >&2 "No PyQT package available. Cannot install."
  exit 1
fi

Packages=(
  python3
  python3-xdg
  "$PyQtPackage"
)

# Install packages
sudo apt install -y "${Packages[@]}"

# Install the browserSelector.desktop file in a place where your DE can find it
mkdir -p ~/.local/share/applications ~/
ActualPath=$(readlink -e "$Dir/../source/browserSelector.py")
awk -v ActualPath="$ActualPath" '
  /^Exec=/ {
    print "Exec=" ActualPath " %U"
    next
  }
  { print }
' "$Dir/browserSelector.desktop" >~/.local/share/applications/browserSelector.desktop
